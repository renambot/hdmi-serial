//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2016
// 
// Luc Renambot - renambot@gmail.com

"use strict";

var commander  = require('commander');      // parsing command-line arguments
var serialport = require("serialport");
var version    = require('./package.json').version;

// Example: node start.js -d /dev/cu.usbserial-AJ03AB01 -c a1 -c b2 -c save

commander
	.version(version)
	.option('-d, --device <device>',   'Specify device to use', '/dev/usb0')
	.option('-l, --list_devices',      'List serial devices')
	.option('-c, --command <command>', 'Send a command to the device (repeatable)', comms, [])
	.parse(process.argv);

function comms(val, arr) {
	arr.push(val);
	return arr;
}

if (commander.list_devices) {
	console.log('List devices:');
	console.log('-------------');
	serialport.list(function (err, ports) {
		if (err) {
			console.error("Error listing devices", err);
			process.exit(1);
		}
		ports.forEach(function(port) {
			console.log("Device:", port.comName);
			if (port.pnpId) {
				console.log("\tID", port.pnpId);
			}
			if (port.manufacturer) {
				console.log("\tManufacturer", port.manufacturer);				
			}
		});
	});

	// Wait to get a reply
	setTimeout(function() {
		process.exit(0);
	}, 1000);

} else {

	var port = new serialport.SerialPort(commander.device, {
		baudrate: 9600,
		dataBits: 8,
		parity: 'none',
		stopBits: 1,
	});

	port.on( "error", function( msg ) {
		console.log("error: " + msg );
	});

	port.on('close', function (err) {
		console.log('Port closed', commander.device);
	});

	port.on("data", function (data) {
		console.log("Got: " + data);
		// port.close();
	});

	port.on('open', function () {
		console.log('Opened', commander.device);

		var buffer;
		var numCommand = commander.command.length;
		var sent = 0;
		var received = 0;
		commander.command.forEach(function(acommand) {

			switch (acommand) {
				case 'on':
				case 'off':
					buffer = genOnOff();
					break;

				case 'save':
					buffer = genSave();
					break;

				case 'a1':
				case 'A1':
					buffer = genA1();
					break;
				case 'a2':
				case 'A2':
					buffer = genA2();
					break;
				case 'a3':
				case 'A3':
					buffer = genA3();
					break;
				case 'a4':
				case 'A4':
					buffer = genA4();
					break;

				case 'b1':
				case 'B1':
					buffer = genB1();
					break;
				case 'b2':
				case 'B2':
					buffer = genB2();
					break;
				case 'b3':
				case 'B3':
					buffer = genB3();
					break;
				case 'b4':
				case 'B4':
					buffer = genB4();
					break;

				default:
					console.log('Command unknown', commander.command);
					process.exit(0);
					break;
			}

			port.drain(function () {
				console.log('... drained');
				received++;
			});

			port.write(buffer, function (err, result) {
				if (err) {
					console.log('Error while sending message : ', err);
				}
				if (result) {
					if (result === 4) {
						console.log('Command sent', acommand);
					} else {
						console.log('Error sending command', acommand, ':', result);
					}
					sent++;
					// uSleep(500000);
					sSleep(1);
				}
				if (sent === numCommand) {
					process.exit(0);					
				}
			});

		});


	});

}

function sSleep(s) {
	var e = new Date().getTime() + (s * 1000);
	while (new Date().getTime() <= e) {
		// wait
		;
	}
}

function uSleep(s) {
	var e = new Date().getTime() + (s / 1000);
	while (new Date().getTime() <= e) {
		// wait
		;
	}
}


function genOnOff() {
	var buffer = new Buffer(4);
	buffer[0] = 0x10;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}
function genSave() {
	var buffer = new Buffer(4);
	buffer[0] = 0x28;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}

function genA1() {
	var buffer = new Buffer(4);
	buffer[0] = 0x0;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}
function genA2() {
	var buffer = new Buffer(4);
	buffer[0] = 0x1;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}
function genA3() {
	var buffer = new Buffer(4);
	buffer[0] = 0x2;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}
function genA4() {
	var buffer = new Buffer(4);
	buffer[0] = 0x3;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}

function genB1() {
	var buffer = new Buffer(4);
	buffer[0] = 0x4;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}
function genB2() {
	var buffer = new Buffer(4);
	buffer[0] = 0x5;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}
function genB3() {
	var buffer = new Buffer(4);
	buffer[0] = 0x6;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}
function genB4() {
	var buffer = new Buffer(4);
	buffer[0] = 0x7;
	buffer[1] = ~ buffer[0];
	buffer[2] = 0xd5;
	buffer[3] = 0x7b;
	return buffer;
}

