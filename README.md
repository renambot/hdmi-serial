First:
   npm install

Usage:

  node start.js [options]

  Options:

    -h, --help               output usage information
    -V, --version            output the version number
    -d, --device <device>    Specify device to use
    -l, --list_devices       List serial devices
    -c, --command <command>  Send a command to the device (repeatable)


List devices:
	node start.js -l
	
		*Device: /dev/cu.LGMusicFlowP761-BTSPP1
		
		*Device: /dev/cu.LGMusicFlowP761-BTSPP2
		
		*Device: /dev/cu.Bluetooth-Incoming-Port
		
		*Device: /dev/cu.LGMusicFlowP761-BTSPP3
		
		*Device: /dev/cu.usbserial-AJ03AB01
		
		*Manufacturer FTDI

Commands:
	
	* on, off, save
	
	* a1, a2, a3, a4: output a to input x
		(or A1, A2, A3, A4)
	* b1, b2, b3, b4: output b to input y
		(or B1, B2, B3, B4)

Example:

	* node start.js -d /dev/cu.usbserial-AJ03AB01 -c a1 -c b2 -c save

